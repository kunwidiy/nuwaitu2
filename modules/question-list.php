<table class="table table-striped">
    <thead>
      <tr>
        <th>Item</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Apa yang Anda pelajari setelah mengisi kuesioner dan mempelajari materi Cleansing?</td>
      </tr>
      <tr>
        <td>Apa yang menghambat Anda untuk menemukan jodoh Anda dan menikah? </td>
      </tr>
      <tr>
        <td>Perlukah Anda mengikuti workshop Cleansing? </td>
      </tr>
      <tr>
        <td>Apa niat menikah Anda yang sesungguhnya? </td>
      </tr>
      <tr>
        <td>Apa Anda sudah betul-betul siap menikah dan berkeluarga?</td>
      </tr>
      <tr>
        <td>Kenapa Anda wajib menikah? Apa visi misi Anda?</td>
      </tr>
      <tr>
        <td>Apa saja kewajiban suami yang Anda pahami?</td>
      </tr>
      <tr>
        <td>Apa saja kewajiban istri yang Anda pahami?</td>
      </tr>
      <tr>
        <td>Bagaimana cara Anda bisa mengontrol emosi Anda sendiri, terutama ketika situasi dan kondisi sedang memanas?</td>
      </tr>
      <tr>
        <td>Persiapan apa saja yang Anda siapkan setelah mempelajari semua materi di Nuwaitu?</td>
      </tr>
      <tr>
        <td>Apa yang sudah Anda siapkan untuk memenuhi kebutuhan HOUSE keluarga Anda kelak?</td>
      </tr>
      <tr>
        <td>Apa yang sudah Anda siapkan untuk memenuhi kebutuhan HOME keluarga Anda kelak?</td>
      </tr>
      <tr>
        <td>Apa makna pekerjaan buat Anda? Kenapa Anda perlu bekerja?</td>
      </tr>
      <tr>
        <td>Apa makna berkeluarga buat Anda? Kenapa Anda perlu dan penting memiliki anak?</td>
      </tr>
      <tr>
        <td>Apakah Anda sudah memutuskan prioritas Anda dalam hidup, mana yang lebih penting? misal kondisi Anda hanya bisa memilih, antara karier atau keluarga</td>
      </tr>
      <tr>
        <td>Kriteria pasangan seperti apa yang Anda butuhkan untuk membangun Rumah Tangga Surga?</td>
      </tr>
      <tr>
        <td>Bagaimana cara Anda memandang WEDDING dan MARRIAGE? Mana yg lebih prioritas Anda penuhi?</td>
      </tr>
    </tbody>
  </table>