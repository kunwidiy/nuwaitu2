<form id="search-form" action="" method="POST" role="form">
      <div id="" class="row">
        <div id="" class="col-sm-2">
          <div class="form-group">
            <label for="">Status</label>
            
            <select class="form-control" id="" name="status">
                <option value="0">Belum Menikah</option>
                <option value="1">Menikah</option>
                <option value="2">Janda Duda</option>
            </select>
          </div>
        </div>
        <div id="" class="col-sm-2">
          <div class="form-group">
            <label for="">Pendidikan</label>
            <select class="form-control" id="" name="education">
                  <option value="0">Tidak Sekolah</option>
                  <option value="1">SD</option>
                  <option value="2">SMP</option>
                  <option value="3">SMA</option>
                  <option value="4">S1</option>
                  <option value="5">S2</option>
                  <option value="6">S3</option>
            </select>
          </div>
        </div>
        <div id="" class="col-sm-2">
          <div class="form-group">
            <label for="">Lokasi</label>
            <select class="form-control" id="" name="location">
                  <option value="0">Bandung</option>
                  <option value="1">Jakarta</option>
                  <option value="2">Ciamis</option>
                  <option value="3">Malang</option>
                  <option value="4">Yogyakarta</option>
            </select>
          </div>
        </div>
        <div id="" class="col-sm-2">
          <div class="form-group">
            <label for="">Umur dari</label>
            <input type="number" name="age[start]" class="form-control" min="17" max="50" value="" placeholder="17">
          </div>
        </div>

        <div id="" class="col-sm-2">
          <div class="form-group">
            <label for="">sampai Umur</label>
            <input type="number" name="age[start]" class="form-control" min="17" max="50" value="" placeholder="50">
          </div>
        </div>
        <div id="" class="col-sm-2">
          <label for="">&nbsp;</label>
          <button type="submit" class="btn btn-pink btn-block">Temukan</button>
        </div>
      </div>
    </form>