		<div class="col-md-offset-1 col-md-10">
			<div class="row">
				<div class="col-md-6 wow animated fadeInUp">
					<div class="nu-pricing hvr-float-shadow">
						<div class="head-kprice">
							<h3><span class="nu-price-icon"><i class="fa fa-cube"></i></span> BASIC</h3>
						</div>
						<div class="body-kprice">
							<p>
								<span class="nu-pricetag">Rp. 100.000</span>
								/Bulan
							</p>
							<ul class="nu-feat-list">
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-times-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-times-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-times-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-times-circle"></i> All Bonus Points</li>
							</ul>
						</div>
						<div class="footer-kprice">
							<p><a class="btn btn-lg btn-block btn-pink" href="#">Daftar</a></p>
						</div>
					</div>
				</div>

				<div class="col-md-6 wow animated fadeInUp">
					<div class="nu-pricing hvr-float-shadow">
						<div class="head-kprice">
							<h3><span class="nu-price-icon"><i class="fa fa-cubes"></i></span> PREMIUM</h3>
						</div>
						<div class="body-kprice">
							<p>
								<span class="nu-pricetag">Rp. 250.000</span>
								/Bulan
							</p>
							<ul class="nu-feat-list">
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-times-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-check-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-times-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-times-circle"></i> All Bonus Points</li>
								<li><i class="fa fa-check-circle"></i> FREE TRAIL 1 MONTH</li>
							</ul>
						</div>
						<div class="footer-kprice">
							<p><a class="btn btn-lg btn-block btn-grape" href="#">Daftar</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>