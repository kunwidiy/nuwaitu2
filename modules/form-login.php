        <form action="" method="POST" role="form" id="login-form">
          <legend>Nuwaitu Login</legend>
        
          <div class="form-group">
            <label for="">Email</label>
            <input type="email" class="form-control input-lg" id="" name="email">
          </div>

          <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control input-lg" id="" name="password">
          </div>
          <button type="submit" class="btn btn-grape btn-block btn-lg"><i class="fa fa-key"></i> Login</button>
        </form>