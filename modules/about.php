                                    <table class="table table-striped">
                                      <tbody>
                                      
                                        <tr>
                                          <td><i class="fa fa-tag"></i> Nama Lengkap</td>
                                          <td>Abdullah Latuconsina</td>
                                        </tr>

                                        <tr>
                                          <td><i class="fa fa-graduation-cap"></i> Pendidikan</td>
                                          <td>S1 Ilmu Komputer</td>
                                        </tr>

                                        <tr>
                                          <td><i class="fa fa-building-o"></i> Pekerjaan</td>
                                          <td>CEO di PT. SAKLIK</td>
                                        </tr>

                                        <tr>
                                          <td><i class="fa fa-heart-o"></i> Status</td>
                                          <td>Lajang</td>
                                        </tr>

                                        <tr>
                                          <td><i class="fa fa-clock-o"></i> Usia</td>
                                          <td>27 Tahun</td>
                                        </tr>

                                         <tr>
                                          <td><i class="fa fa-child"></i> Anak</td>
                                          <td>0</td>
                                        </tr>
                                
                                        <tr>
                                          <td><i class="fa fa-map-marker"></i> Lokasi</td>
                                          <td>Bandung, Jawa Barat, Indonesia</td>
                                        </tr>

                                      </tbody>
                                    </table>