<!DOCTYPE html>
<html lang="en-US">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="nuwaitu, wedding, love, islam, syari, jodoh">
    
    <title>Nuwaitu - HTML Template</title>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <link rel="shortcut icon" href="assets/img/favicon.png">
    
    <!-- Core CSS -->
    <link type="text/css" rel="stylesheet" href="assets/css/pluggin.css"><!-- Bootstrap + animate -->
    <link type="text/css" rel="stylesheet" href="assets/css/nuwaitu.css"><!-- nuwaitu style -->

    <script src="assets/js/modernizr-2.6.2.min.js"></script>
       
</head>


<body>

    <!-- header Section -->
    <header>
        <div class="nu-header-section">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse anti-round" role="navigation">
                <div class="container">

                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"><img class="nu-logo" src="assets/img/logo.png" alt="nuwaitu"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="dashboard.php">DASHBOARD</a>
                            </li>
                            <li>
                                <a href="nuwaitu.php">NUWAITU</a>
                            </li>
                            <li>
                                <a href="nu-plus.php">NU+</a>
                            </li>
                            <li>
                                <a href="jdaku.php">JDAKU</a>
                            </li>

                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Abdul <span class="caret"></span></a>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Pesan</a></li>
                                <li><a href="#">Edit Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Logout</a></li>
                              </ul>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>
            <!-- End Navigation -->
        </div>
    </header>
    <!-- end header Section -->