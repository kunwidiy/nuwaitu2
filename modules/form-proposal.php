
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Step 1</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Step 2</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Step 3</p>
        </div>
         <div class="stepwizard-step">
            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
            <p>Step 4</p>
        </div>
    </div>
</div>

<form id="proposal-form" action="" method="POST" role="form">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> section 1</h3>

                <!-- 16:9 aspect video embed ratio -->
                <div class="embed-responsive embed-responsive-16by9 nuembed">
                  <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/J_7esUTJpcY" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="form-group">
                  <label>Cita-cita terbesarku adalah meraih ridha-Nya dengan dimasukkan ke dalam surga. Karena itu sebelum meninggal, saya akan……..</label>
                  <textarea type="text" required="required" class="form-control" name="life_goal" rows="10"></textarea>
                </div>

                <button class="btn btn-primary nextBtn pull-right" type="button" >Selanjutnya</button>
            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>section 2</h3>

                <!-- 16:9 aspect video embed ratio -->
                <div class="embed-responsive embed-responsive-16by9 nuembed">
                  <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/J_7esUTJpcY" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="form-group">
                  <label>Di akhirat kelak,yang ingin saya lakukan adalah……</label>
                  <textarea type="text" required="required" class="form-control" name="after_life_goal" rows="10" id=""></textarea>
                </div>

                <div class="form-group">
                  <label>Untuk mencapainya,saya berniat membangun keluarga dengan prinsip-prinsip dan nilai-nilai…</label>
                  <textarea type="text" required="required" class="form-control" name="values" rows="10" id=""></textarea>
                </div>

                <button class="btn btn-default prevBtn pull-left" type="button" >Kembali</button>
                <button class="btn btn-primary nextBtn pull-right" type="button" >Selanjutnya</button>
            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>section 3</h3>

                <!-- 16:9 aspect video embed ratio -->
                <div class="embed-responsive embed-responsive-16by9 nuembed">
                  <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/J_7esUTJpcY" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="form-group">
                  <label>Nah, Sejujurnya kondisi saya saat ini…………</label>
                  <textarea type="text" required="required" class="form-control" name="condition" rows="10" id=""></textarea>
                </div>
                <button class="btn btn-default prevBtn pull-left" type="button" >Kembali</button>
                <button class="btn btn-primary nextBtn pull-right" type="button" >Selanjutnya</button>
            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 4</h3>

                <!-- 16:9 aspect video embed ratio -->
                <div class="embed-responsive embed-responsive-16by9 nuembed">
                  <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/J_7esUTJpcY" frameborder="0" allowfullscreen></iframe>
                </div>

                <div class="form-group">
                  <label>Karena itu rencana hidup saya 5 Tahun yang akan datang………….</label>
                  <textarea type="text" required="required" class="form-control" name="y5_plan" rows="10" id=""></textarea>
                </div>

                <button class="btn btn-default prevBtn pull-left" type="button" >Kembali</button>
                <button class="btn btn-success pull-right" type="submit">Finish!</button>
            </div>
        </div>
    </div>
</form>