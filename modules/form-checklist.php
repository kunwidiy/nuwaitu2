<form id="checklist-form" action="" method="POST" role="form">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Item</th>
          <th class="text-center" style="width:5%;">Belum</th>
          <th class="text-center" style="width:5%;">Sudah</th>
          <th style="min-width:120px;">Tanggal</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Sudahkah Anda memohon restu dari orangtua?</td>
          <td class="text-center"><input name="_0[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_0[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_0[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Anda untuk menjalani semua proses di Nuwaitu? </td>
          <td class="text-center"><input name="_1[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_1[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_1[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda mempelajari materi Cleansing? </td>
          <td class="text-center"><input name="_2[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_2[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_2[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda mempelajari semua materi dalam sesi Upgrading? </td>
          <td class="text-center"><input name="_3[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_3[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_3[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda memastikan secara bulat niat menikah Anda sudah betul-betul kokoh? </td>
          <td class="text-center"><input name="_4[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_4[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_4[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda mengikuti workshop Cleansing, jika ternyata Anda membutuhkannya? </td>
          <td class="text-center"><input name="_5[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_5[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_5[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda memahami kewajiban sebagai suami dan istri? </td>
          <td class="text-center"><input name="_6[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_6[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_6[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda siap memenuhi kebutuhan House dan Home keluarga Anda? </td>
          <td class="text-center"><input name="_7[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_7[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_7[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda siap untuk mencari nafkah sekalipun istri Anda tidak bekerja? (khusus untuk pria) </td>
          <td class="text-center"><input name="_8[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_8[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_8[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda siap, di tengah lelahnya aktivitas rutinan, Anda meluangkan waktu untuk menemani pasangan dan anak Anda? </td>
          <td class="text-center"><input name="_9[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_9[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_9[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda memiliki skill untuk mengelola emosi Anda, sekalipun ketika emosi Anda naik? </td>
          <td class="text-center"><input name="_10[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_10[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_10[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda mempelajari tahapan penyusunan proposal pernikahan?</td>
          <td class="text-center"><input name="_11[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_11[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_11[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda menyusun proposal pernikahan Anda? </td>
          <td class="text-center"><input name="_12[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_12[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_12[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda menentukan prioritas Anda dalam menentukan kriteria pasangan seperti apa yang tepat untuk diajak kerjasama membangun Rumah Tangga Surga Anda? </td>
          <td class="text-center"><input name="_13[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_13[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_13[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda menyepakati bersama calon pasangan Anda terkait rencana hidup 5, 10, 15 tahun ke depan akan seperti apa, antara visi hidup, pembagian peran, serta rules dalam rumah Anda? </td>
          <td class="text-center"><input name="_14[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_14[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_14[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda menyepakati bersama calon pasangan Anda terkait bagaimana cara menyeimbangkan antara karier dan keluarga Anda? </td>
          <td class="text-center"><input name="_15[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_15[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_15[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Jika sudah memilih dan sudah ada persetujuan dari calon, sudahkah Anda kontak walinya untuk melamar? </td>
          <td class="text-center"><input name="_16[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_16[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_16[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda menentukan bersama tanggal pernikahan ANda? </td>
          <td class="text-center"><input name="_17[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_17[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_17[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda mengkomunikasikan dan bernegosiasi dengan baik kepada orangtua Anda terkait calon Anda, rencana lamaran, dan rencana pernikahan Anda?</td>
          <td class="text-center"><input name="_18[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_18[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_18[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda menyepakati bersama calon pasangan Anda tentang rencana akad dan resepsi yang akan diselenggarakan? </td>
          <td class="text-center"><input name="_19[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_19[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_19[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Setelah memahami antara WEDDING &amp; MARRIAGE, sudahkah Anda menyepakati bersama calon dan keempat orangtua Anda tentang konsep resepsi Anda? </td>
          <td class="text-center"><input name="_20[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_20[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_20[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda memastikan tidak adanya hutang dan riba dalam resepsi yang akan Anda susun? </td>
          <td class="text-center"><input name="_21[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_21[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_21[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda memastikan bahwa selama proses setelah lamaran, Anda tetap menjaga hati  Anda dari bisikan setan yang selalu mengajak berduaan, padahal belum halal? 23 Sudahkah Anda menyiapkan mahar yang diminta calon Anda? (khusus pria) </td>
          <td class="text-center"><input name="_22[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_22[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_22[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda meminta hak mahar Anda yang tidak memberatkan calon Anda sekaligus penting? (khusus wanita)</td>
          <td class="text-center"><input name="_23[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_23[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_23[date]" type="text" class="form-control" value=""></td>
        </tr>
        <tr>
          <td>Sudahkah Anda persiapkan perbekalan dengan ilmu parenting?</td>
          <td class="text-center"><input name="_24[status]" type="radio" value="0" checked=""> </td>
          <td class="text-center"><input name="_24[status]" type="radio" value="1"> </td>
          <td class="text-center"><input name="_24[date]" type="text" class="form-control" value=""></td>
        </tr>
      </tbody>
    </table>

     <div class="btn-form">
        <button type="submit" class="btn btn-pink btn-lg"><i class="fa fa-paper-plane-o"></i> Submit</button>
      </div>
  </form>