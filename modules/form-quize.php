          <form id="quiz-form" action="" method="POST" role="form">
            <div id="quiz-container">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th style="width:65%;">Pertanyaan</th>
                    <th style="width:7%;"><img src="assets/img/qu1.png" alt="sangat tidak setuju"></th>
                    <th style="width:7%;"><img src="assets/img/qu2.png" alt="sangat tidak setuju"></th>
                    <th style="width:7%;"><img src="assets/img/qu3.png" alt="sangat tidak setuju"></th>
                    <th style="width:7%;"><img src="assets/img/qu4.png" alt="sangat tidak setuju"></th>
                    <th style="width:7%;"><img src="assets/img/qu5.png" alt="sangat tidak setuju"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Saya Ingin Mendapatkan Pasangan yang mengerti diri saya seutuhnya</td>
                    <td class="text-center"><input name="0" type="radio" value="0"> </td>
                    <td class="text-center"><input name="0" type="radio" value="1"> </td>
                    <td class="text-center"><input name="0" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="0" type="radio" value="3"> </td>
                    <td class="text-center"><input name="0" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya lelah diundang ke pesta pernikahan sahabat-sahabat saya</td>
                    <td class="text-center"><input name="1" type="radio" value="0"> </td>
                    <td class="text-center"><input name="1" type="radio" value="1"> </td>
                    <td class="text-center"><input name="1" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="1" type="radio" value="3"> </td>
                    <td class="text-center"><input name="1" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya tidak nyaman di rumah orangtua sehingga menikah saya anggap sebagai solusi permasalahan saya</td>
                    <td class="text-center"><input name="2" type="radio" value="0"> </td>
                    <td class="text-center"><input name="2" type="radio" value="1"> </td>
                    <td class="text-center"><input name="2" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="2" type="radio" value="3"> </td>
                    <td class="text-center"><input name="2" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya butuh teman hidup karena sudah bosan hidup sendiri</td>
                    <td class="text-center"><input name="3" type="radio" value="0"> </td>
                    <td class="text-center"><input name="3" type="radio" value="1"> </td>
                    <td class="text-center"><input name="3" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="3" type="radio" value="3"> </td>
                    <td class="text-center"><input name="3" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Mantan menikah? Aku juga bisaaaa</td>
                    <td class="text-center"><input name="4" type="radio" value="0"> </td>
                    <td class="text-center"><input name="4" type="radio" value="1"> </td>
                    <td class="text-center"><input name="4" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="4" type="radio" value="3"> </td>
                    <td class="text-center"><input name="4" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya menikah untuk mengejar barokahNya, dan saya sudah betul betul paham maknanya</td>
                    <td class="text-center"><input name="5" type="radio" value="0"> </td>
                    <td class="text-center"><input name="5" type="radio" value="1"> </td>
                    <td class="text-center"><input name="5" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="5" type="radio" value="3"> </td>
                    <td class="text-center"><input name="5" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya terhalang restu menikah oleh kakak. </td>
                    <td class="text-center"><input name="6" type="radio" value="0"> </td>
                    <td class="text-center"><input name="6" type="radio" value="1"> </td>
                    <td class="text-center"><input name="6" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="6" type="radio" value="3"> </td>
                    <td class="text-center"><input name="6" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Tiap minggu, rasanya panas melihat janur kuning bertebaran di jalan</td>
                    <td class="text-center"><input name="7" type="radio" value="0"> </td>
                    <td class="text-center"><input name="7" type="radio" value="1"> </td>
                    <td class="text-center"><input name="7" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="7" type="radio" value="3"> </td>
                    <td class="text-center"><input name="7" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya yakin rezeki saya setelah menikah Allah yg atur, setelah saya berusaha semaksimal saya</td>
                    <td class="text-center"><input name="8" type="radio" value="0"> </td>
                    <td class="text-center"><input name="8" type="radio" value="1"> </td>
                    <td class="text-center"><input name="8" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="8" type="radio" value="3"> </td>
                    <td class="text-center"><input name="8" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Orangtua saya susah sekali menurunkan restu pernikahannya</td>
                    <td class="text-center"><input name="9" type="radio" value="0"> </td>
                    <td class="text-center"><input name="9" type="radio" value="1"> </td>
                    <td class="text-center"><input name="9" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="9" type="radio" value="3"> </td>
                    <td class="text-center"><input name="9" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya sudah berharap untuk menggendong dan membesarkan anak saya sendiri</td>
                    <td class="text-center"><input name="10" type="radio" value="0"> </td>
                    <td class="text-center"><input name="10" type="radio" value="1"> </td>
                    <td class="text-center"><input name="10" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="10" type="radio" value="3"> </td>
                    <td class="text-center"><input name="10" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya sudah rutin mempelajari ilmu parenting baik dari buku, online, atau mengikuti seminar/workshopnya</td>
                    <td class="text-center"><input name="11" type="radio" value="0"> </td>
                    <td class="text-center"><input name="11" type="radio" value="1"> </td>
                    <td class="text-center"><input name="11" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="11" type="radio" value="3"> </td>
                    <td class="text-center"><input name="11" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya telah memahami seutuhnya tugas dan paran suami/istri dan saya siap melakukannya</td>
                    <td class="text-center"><input name="12" type="radio" value="0"> </td>
                    <td class="text-center"><input name="12" type="radio" value="1"> </td>
                    <td class="text-center"><input name="12" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="12" type="radio" value="3"> </td>
                    <td class="text-center"><input name="12" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Jika teman seangkatan menyediakan piala bergilir ketika pernikahan, saya tidak mau menjadi orang terakhir yang mendapatkan piala tersebut</td>
                    <td class="text-center"><input name="13" type="radio" value="0"> </td>
                    <td class="text-center"><input name="13" type="radio" value="1"> </td>
                    <td class="text-center"><input name="13" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="13" type="radio" value="3"> </td>
                    <td class="text-center"><input name="13" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Tak ada yang tahu hari esok, jika setelah menikah justru saya diuji dengan rezeki yang sedans dipersempit, saya tetap siap berjuang bersama pasangan saya</td>
                    <td class="text-center"><input name="14" type="radio" value="0"> </td>
                    <td class="text-center"><input name="14" type="radio" value="1"> </td>
                    <td class="text-center"><input name="14" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="14" type="radio" value="3"> </td>
                    <td class="text-center"><input name="14" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Bagi saya, proses perkenalan dengan pasangan saya adalah proses yang perlu dilakukan setiap hari setelah menikah</td>
                    <td class="text-center"><input name="15" type="radio" value="0"> </td>
                    <td class="text-center"><input name="15" type="radio" value="1"> </td>
                    <td class="text-center"><input name="15" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="15" type="radio" value="3"> </td>
                    <td class="text-center"><input name="15" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Saya masih agak takut untuk menjalani pernikahan karena contoh nyata kehidupan pernikahan di sekitar saya jauh dari pernikahan</td>
                    <td class="text-center"><input name="16" type="radio" value="0"> </td>
                    <td class="text-center"><input name="16" type="radio" value="1"> </td>
                    <td class="text-center"><input name="16" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="16" type="radio" value="3"> </td>
                    <td class="text-center"><input name="16" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Setelah menikah, maka happily ever after</td>
                    <td class="text-center"><input name="17" type="radio" value="0"> </td>
                    <td class="text-center"><input name="17" type="radio" value="1"> </td>
                    <td class="text-center"><input name="17" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="17" type="radio" value="3"> </td>
                    <td class="text-center"><input name="17" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Perlu memiliki rumah dan mobil pribadi dulu agar terbilang siap menikah</td>
                    <td class="text-center"><input name="18" type="radio" value="0"> </td>
                    <td class="text-center"><input name="18" type="radio" value="1"> </td>
                    <td class="text-center"><input name="18" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="18" type="radio" value="3"> </td>
                    <td class="text-center"><input name="18" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Menikah dan memiliki keturunan akan mengganggu kemajuan karier saya</td>
                    <td class="text-center"><input name="19" type="radio" value="0"> </td>
                    <td class="text-center"><input name="19" type="radio" value="1"> </td>
                    <td class="text-center"><input name="19" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="19" type="radio" value="3"> </td>
                    <td class="text-center"><input name="19" type="radio" value="4"> </td>
                  </tr>
                  <tr>
                    <td>Sesibuk-sibuknya saya dalam bekerja, saya akan selalu meluangkan waktu saya minimal satu jam untuk membersamai pasangan dan anak saya</td>
                    <td class="text-center"><input name="20" type="radio" value="0"> </td>
                    <td class="text-center"><input name="20" type="radio" value="1"> </td>
                    <td class="text-center"><input name="20" type="radio" value="2" checked=""> </td>
                    <td class="text-center"><input name="20" type="radio" value="3"> </td>
                    <td class="text-center"><input name="20" type="radio" value="4"> </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <script type="text/x-handlebars-template" id="quiz-template">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th style="width:75%;">Pertanyaan</th>
                    <th style="width:5%;">Sangat Tidak Setuju</th>
                    <th style="width:5%;"></th>
                    <th style="width:5%;">Netral</th>
                    <th style="width:5%;"></th>
                    <th style="width:5%;">Sangat Setuju</th>
                  </tr>
                </thead>
                <tbody>
                  {{#each items}}
                  <tr>
                    <td>{{title}}</td>
                    <td class="text-center"><input name="{{@key}}" type="radio" value="0"> </td>
                    <td class="text-center"><input name="{{@key}}" type="radio" value="1"> </td>
                    <td class="text-center"><input name="{{@key}}" type="radio" value="2" checked> </td>
                    <td class="text-center"><input name="{{@key}}" type="radio" value="3"> </td>
                    <td class="text-center"><input name="{{@key}}" type="radio" value="4"> </td>
                  </tr>
                  {{/each}}
                </tbody>
              </table>
            </script> 

            <div class="btn-form">
              <button type="submit" class="btn btn-pink btn-lg"><i class="fa fa-paper-plane-o"></i> Submit</button>
            </div> 

          </form>