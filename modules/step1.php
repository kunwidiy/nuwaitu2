
    
    <div class="step">
        <div id="div1" class="nu-steps" onclick="javascript: resetActive(event, 11.5);">
            <span class="fa fa-heart"></span>
            <p>Register</p>
        </div>
        <div class="nu-steps active" onclick="javascript: resetActive(event, 25);">
            <span class="fa fa-pencil"></span>
            <p>Quiz</p>
        </div>
        <div class="nu-steps" onclick="javascript: resetActive(event, 45);">
            <span class="fa fa-book"></span>
            <p>Proposal</p>
        </div>
        <div class="nu-steps" onclick="javascript: resetActive(event, 55);">
            <span class="fa fa-eye"></span>
            <p>Cari jodoh</p>
        </div>
        <div class="nu-steps" onclick="javascript: resetActive(event, 80);">
            <span class="fa fa-comments-o"></span>
            <p>Ta'aruf</p>
        </div>
        <div class="nu-steps" onclick="javascript: resetActive(event, 85);">
            <span class="fa fa-check-square-o"></span>
            <p>Checklist</p>
        </div>
        <div id="last" class="nu-steps" onclick="javascript: resetActive(event, 100);">
            <span class="fa fa-check-circle-o"></span>
            <p>Question list</p>
        </div>

        <!-- progress bar -->
        <div class="progress" id="progress1">
	        <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">                
	        </div>
	        <span class="progress-type">Overall Progress</span>
	        <span class="progress-completed">25%</span>
	    </div> 
        <!-- end progress bar -->
    </div>
    
