

	<!-- short list -->
	<ul class="nu-short-list">
		<li>
			<div class="image">
				<a href="#">
					<img src="uploads/user1.png" alt="akhwat name">
				</a>
			</div>

			<div class="text">
				<h5>Esih, 24 <span><i class="fa fa-map-marker"></i> Bandung, Mahasiswa</span></h5>
				<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-frown-o"></i> Batalkan</a>
			</div>
		</li>

		<li>
			<div class="image">
				<a href="#">
					<img src="uploads/user2.png" alt="akhwat name">
				</a>
			</div>

			<div class="text">
				<h5>Humaira, 27 <span><i class="fa fa-map-marker"></i> Jakarta, Guru ngaji</span></h5>
				<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-frown-o"></i> Batalkan</a>
			</div>
		</li>

		<li>
			<div class="image">
				<a href="#">
					<img src="uploads/user4.png" alt="akhwat name">
				</a>
			</div>

			<div class="text">
				<h5>Ainun, 30 <span><i class="fa fa-map-marker"></i> Aceh, Dokter cinta</span></h5>
				<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-frown-o"></i> Batalkan</a>
			</div>
		</li>
	</ul>
	<!-- end short list -->