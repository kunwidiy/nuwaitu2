
  
<form id="profile-form" action="" method="POST" role="form">
   <fieldset>

    <div class="form-group col-md-12">
      <input class="form-control" id="nu-profil-pic1" type="file">
    </div>

    <div class="form-group col-md-6">
      <label for="">Nama Panggilan</label>
      <input type="text" class="form-control" id="" name="display_name" value="">
    </div>

    <div class="form-group col-md-6">
      <label for="">Nama Lengkap</label>
      <input type="text" class="form-control" id="" name="full_name" value="">
    </div>

    <div class="form-group col-md-6">
      <label for="">Jenis Kelamin</label>
      <select class="form-control" id="" name="sex">
          <option value="0">Perempuan</option>
          <option value="1">Laki Laki</option>
      </select>
    </div>
    
    <div class="form-group col-md-6">
      <label for="">Pendidikan</label>
      <select class="form-control" id="" name="education">
          <option value="0">Tidak Sekolah</option>
          <option value="1">SD</option>
          <option value="2">SMP</option>
          <option value="3">SMA</option>
          <option value="4">S1</option>
          <option value="5">S2</option>
          <option value="6">S3</option>
      </select>
    </div>

    <div class="form-group col-md-6">
      <label for="">Pekerjaan</label>
      <input type="text" class="form-control" id="" name="job" value="">
    </div>

    <div class="form-group col-md-6">
      <label for="">Tanggal lahir</label>
      <input type="text" class="form-control date-picker" id="" name="date_of_birth" value="">
    </div>

    <div class="form-group col-md-6">
      <label for="">Lokasi</label>
      <select class="form-control" id="" name="location">
          <option value="0">Bandung</option>
          <option value="1">Jakarta</option>
          <option value="2">Ciamis</option>
          <option value="3">Malang</option>
          <option value="4">Yogyakarta</option>
      </select>
    </div>

    <div class="form-group col-md-6">
      <label for="">Status Menikah</label>
      <select class="form-control" id="" name="status">
          <option value="0" selected="selected">Belum Menikah</option>
          <option value="1">Menikah</option>
          <option value="2">Janda Duda</option>
      </select>
    </div>

    
      <div class="form-group col-md-6">
        <label for="">Jumlah Anak</label>
        <input type="number" class="form-control" id="" name="kids" value="">
      </div>

      <div class="form-group col-md-6">
        <div class="checkbox">
          <label>
            <input type="checkbox"> Saya menyatakan bahwa wali saya sudah menyetujui kepesertaan saya di Nuwaitu
          </label>
        </div>
      </div>
   
   
    <div class="btn-form col-md-12">
      <button type="submit" class="btn btn-pink btn-lg"><i class="fa fa-paper-plane-o"></i> Submit</button>
    </div> 

  </fieldset>
</form>