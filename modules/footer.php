    <!-- bottom block -->
    <div class="nu-quote-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="nu-quotes">
                        <h3>Menikahi pasangan yang salah lebih buruk daripada tidak menikah.</h3>
                        <p>Memilih pasangan yang salah dan dipilih pasangan yang salah keduanya <br> sama-sama parah -
                        <span>Felix Siauw, <strong>Penulis best seller Udah Putusin Ajah1</strong></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end bottom block -->

    <div class="clearfix"></div>

    <!-- Footer Section -->
    <footer class="nu-footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="nu-bottom-box">
                        <h4>ABOUT US</h4>
                        <p>Here you can put your customized short information, great!</p>
                        <p>
                            <span class="block"> <i class="fa fa-envelope"></i> yourname@youeemail.com</span>
                            <span class="block"> <i class="fa fa-phone"></i> +62(21)23456789</span>
                            <span class="block"> <i class="fa fa-print"></i> +62(21)23456788</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="nu-bottom-box">
                        <h4>FIND US ON</h4>
                        <span class="nu-social"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></span>
                        <span class="nu-social"><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></span>
                        <span class="nu-social"><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="nu-bottom-box">
                        <h4><i class="fa fa-facebook"></i> LATEST ACTIVITY</h4>
            
                    </div>
                </div>
            </div>

            
            <div class="nu-copyright">
                <div class="row">
                    <div class="col-md-6">
                        <a href="#"><img src="assets/img/logo2.png" alt="nuwaitu"></a>
                    </div>
                    <div class="col-md-6">
                        <p class="nu-copytext">Copyright <i class="fa fa-copyright"></i> 2016. Designed by <a href="#">Nuwaitu</a></p>
                    </div>
                </div>
                <a class="nuwaitu-backtop" href="#"><i class="fa fa-chevron-circle-up"></i></a>
            </div>
        </div>
    </footer>
    <!-- End Footer Section -->

</body>

  <!-- Core JavaScript Files -->
  <script src="assets/js/jquery-1.11.1.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/owl.carousel.min.js"></script>
  <script src="assets/js/wow.min.js"></script>
  <script src="assets/js/fileinput.min.js"></script>
  <script src="assets/js/jquery.sortable.js"></script>
  
  <!-- Nuwaitu JavaScript File -->
  <script src="assets/js/nuwaitu.js"></script>

</html>