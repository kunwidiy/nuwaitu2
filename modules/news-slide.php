                        <div id="id-featured-news" class="owl-carousel owl-theme">

                            <div class="item">
                              <div class="jdaku-feat-news">
                                <div class="news-img">
                                    <img src="uploads/news1.png" alt="news title">
                                </div>
                                
                                <div class="news-title">
                                    <h5>Using slider as media blog content</h5>
                                    <p>Relationships are made up of defising moments, both big and small. Some love stories hit by a romantic couples</p>
                                    <p class="readmore"><a class="btn btn-pink" href="#">READ MORE</a></p>
                                </div>
                              </div>
                            </div> 

                            <div class="item">
                              <div class="jdaku-feat-news">
                                <div class="news-img">
                                    <img src="uploads/news2.jpg" alt="news title">
                                </div>
                                
                                <div class="news-title">
                                    <h5>Another title slider as media blog content</h5>
                                    <p>Relationships are made up of defising moments, both big and small. Some love stories hit by a romantic couples</p>
                                    <p class="readmore"><a class="btn btn-pink" href="#">READ MORE</a></p>
                                </div>
                              </div>
                            </div> 

                            <div class="item">
                              <div class="jdaku-feat-news">
                                <div class="news-img">
                                    <img src="uploads/news3.jpg" alt="news title">
                                </div>
                                
                                <div class="news-title">
                                    <h5>The last title as media blog content</h5>
                                    <p>Relationships are made up of defising moments, both big and small. Some love stories hit by a romantic couples</p>
                                    <p class="readmore"><a class="btn btn-pink" href="#">READ MORE</a></p>
                                </div>
                              </div>
                            </div> 

                
                        </div>