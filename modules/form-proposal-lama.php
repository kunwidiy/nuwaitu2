<form id="proposal-form" action="" method="POST" role="form">
    <legend class="nu-legend col-md-12">Pengantar</legend>

    <div class="form-group col-md-12">
      <label>Cita-cita terbesarku adalah meraih ridha-Nya dengan dimasukkan ke dalam surga. Karena itu sebelum meninggal, saya akan……..</label>
      <textarea class="form-control" name="life_goal" rows="10" id=""></textarea>
    </div>
  

    <div class="form-group col-md-12">
      <label>Di akhirat kelak,yang ingin saya lakukan adalah……</label>
      <textarea class="form-control" name="after_life_goal" rows="10" id=""></textarea>
    </div>
 

    <div class="form-group col-md-12">
      <label>Untuk mencapainya,saya berniat membangun keluarga dengan prinsip-prinsip dan nilai-nilai…</label>
      <textarea class="form-control" name="values" rows="10" id=""></textarea>
    </div>
  

    <div class="form-group col-md-12">
      <label>Nah, Sejujurnya kondisi saya saat ini…………</label>
      <textarea class="form-control" name="condition" rows="10" id=""></textarea>
    </div>
  

    <div class="form-group col-md-12">
      <label>Karena itu rencana hidup saya 5 Tahun yang akan datang………….</label>
      <textarea class="form-control" name="y5_plan" rows="10" id=""></textarea>
    </div>
  

    <div class="form-group col-md-12">
      <label>Rencana hidup saya 10Tahun yang akan datang………….</label>
      <textarea class="form-control" name="y10_plan" rows="10" id=""></textarea>
    </div>
  

    <div class="form-group col-md-12">
      <label>Sehingga, jika hidup saya layaknya sebuah novel, maka sinopsisnya akan seperti ini…………</label>
      <textarea class="form-control" name="synopsis" rows="10" id=""></textarea>
    </div>
  

    <div class="form-group col-md-12">
      <label>Karena itu,pembagian peran yang saya harapkan dalam pernikahan nanti adalah….</label>
      <textarea class="form-control" name="role" rows="10" id=""></textarea>
    </div>
            
    <legend class="nu-legend col-md-12">Kebutuhan Tetap</legend>

    <div class="form-group col-md-6">
      <label>Makan Sehat</label>
      <select class="form-control" name="monthly[meal]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Kebutuhan Tempat Tinggal</label>
      <select class="form-control" name="monthly[place]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Listrik</label>
      <select class="form-control" name="monthly[electricity]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Air</label>
      <select class="form-control" name="monthly[water]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Sampah</label>
      <select class="form-control" name="monthly[garbage]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Air Minum</label>
      <select class="form-control" name="monthly[drink]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Kebersihan rumah</label>
      <select class="form-control" name="monthly[sanitary]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Kebutuhan Laundry</label>
      <select class="form-control" name="monthly[laundry]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Pendidikan Formal</label>
      <select class="form-control" name="monthly[education_formal]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Pendidikan Non Formal</label>
      <select class="form-control" name="monthly[education_non_formal]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Tabungan</label>
      <select class="form-control" name="monthly[saving]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Rekreasi</label>
      <select class="form-control" name="monthly[recreation]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Transportasi</label>
      <select class="form-control" name="monthly[transportation]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Pulsa</label>
      <select class="form-control" name="monthly[phone_credit]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Buku</label>
      <select class="form-control" name="monthly[books]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
  

    <div class="form-group col-md-6">
      <label>Adik</label>
      <select class="form-control" name="monthly[social]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>

    <legend class="nu-legend col-md-12">Kebutuhan Kondisional</legend>


    <div id="" class="form-group col-md-6">
      <label>Pakaian</label>
      <select class="form-control" name="occasionaly[clothes]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
    <div id="" class="form-group col-md-6">
      <label>Sepatu</label>
      <select class="form-control" name="occasionaly[shoes]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
    <div id="" class="form-group col-md-6">
      <label>Tas</label>
      <select class="form-control" name="occasionaly[bags]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
    <div id="" class="form-group col-md-6">
      <label>Kosmetik</label>
      <select class="form-control" name="occasionaly[cosmetic]">
          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>
    <div id="" class="form-group col-md-12">
      <label>Other</label>
      <select class="form-control" name="occasionaly[other]">

          <option value="100">0 - Rp 100.000</option>
          <option value="200">Rp 200.000</option>
          <option value="300">Rp 300.000</option>
          <option value="400">Rp 400.000</option>
          <option value="500">Rp 400.000 ++</option>
      </select>
    </div>


     <div class="btn-form col-md-12">
        <button type="submit" class="btn btn-pink btn-lg"><i class="fa fa-paper-plane-o"></i> Submit</button>
      </div> 
  </form>