<div class="row">

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser1.jpg" alt="akhwat name">
				<p>Hawa, 20<span>Mahasiswa</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser2.jpg" alt="akhwat name">
				<p>Kebab Turki, 29<span>Pengusaha</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser3.jpg" alt="akhwat name">
				<p>Zahira, 25<span>Karyawati</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser4.jpg" alt="akhwat name">
				<p>Megaria, 21<span>Karyawati</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser5.jpg" alt="akhwat name">
				<p>Neng Imas, 22<span>Freelancer</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser6.jpg" alt="akhwat name">
				<p>Komalasari, 20<span>Designer</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser1.jpg" alt="akhwat name">
				<p>Hayatul, 29<span>Pengusaha</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser2.jpg" alt="akhwat name">
				<p>Purnama, 30<span>Pengusaha</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser1.jpg" alt="akhwat name">
				<p>Hawa, 20<span>Mahasiswa</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser2.jpg" alt="akhwat name">
				<p>Kebab Turki, 29<span>Pengusaha</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser3.jpg" alt="akhwat name">
				<p>Zahira, 25<span>Karyawati</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	<div class="col-md-3">
		<!-- user thumb -->
		<div class="nu-user-thumb">

			<div class="caption">
                <h5>Thumbnail Headline</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.</p>
                <p><a href="" class="label label-info"><i class="fa fa-heart"></i> add to shortlist</a>
                <a href="" class="label label-warning"><i class="fa fa-eye"></i> lihat profile</a></p>
            </div>

			<div class="image">
				<img src="uploads/biguser4.jpg" alt="akhwat name">
				<p>Megaria, 21<span>Karyawati</span></p>
			</div>

		</div>
		<!-- end user thumb -->
	</div>

	

</div>