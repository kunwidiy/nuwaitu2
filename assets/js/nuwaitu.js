//  ====================================================================
//	Theme Name: Etcetra Template
//	Theme URI: http://themeforest.net/user/responsiveexperts
//	Description: This javascript file is using as a settings file for etcetera theme
//	Version: 1.0
//	Author: Priyandiono
//	Tags: wordpress, theme, Compro, Event
//  ====================================================================

//	TABLE OF CONTENTS
//	---------------------------
//	 01. home slider
//	 02. news slider
//	 03. member slider
//	 04. upload photo
//	 05. Scroll To Top

//  ====================================================================

    
    // Bootsrap core
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    $(function () {
      $('[data-toggle="popover"]').popover()
    })

    $('.popover-hover').popover({ 
        placement: 'top',
        title: "Proposal :",
        trigger: 'hover',
        html : true
    });
	
	// -------------------- 01. home slider --------------------
	// ---------------------------------------------------------
	var slideHeight = $(window).height();
	    $('#nu-home-carousel .carousel-inner .item, #nu-home-carousel').css('height',slideHeight);

	    

	    $('.carousel').carousel({
		   interval: 7000
		});


		var wow = new WOW ({
		  offset:       100,        
		  mobile:       false,    
		});
		wow.init();


	// -------------------- 02.news slider ---------------------
	// ---------------------------------------------------------

	$("#id-featured-news").owlCarousel({
               
        slideSpeed : 300,
        paginationSpeed : 400,
        pagination : false,
        singleItem:true,
        autoPlay : true,
        navigation : true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        transitionStyle : "fade"
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false
   
    });

    // -------------------- 03.member slider -------------------
	// ---------------------------------------------------------
    $("#id-nu-members").owlCarousel({
        
        itemsCustom : [
          [0, 2],
          [700, 3],
          [1000, 4],
          [1200, 5],
        ], 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: false,
        pagination : false,
        autoPlay : true,
        navigation : true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        transitionStyle : "backSlide",
        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false
   
    });

    $("#id-dash-members").owlCarousel({
        
        itemsCustom : [
          [0, 2],
          [700, 3],
          [1000, 4],
          [1200, 4],
        ], 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: false,
        pagination : false,
        autoPlay : true,
        navigation : true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        transitionStyle : "backSlide"
   
    });

    $("#id-home-slide2").owlCarousel({
 
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      autoPlay : true,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
    });

    // -------------------- 04.upload photo --------------------
    // ---------------------------------------------------------

    $("#nu-profil-pic1").fileinput({
        initialPreview: ["<img src='assets/img/user.png' class='file-preview-image'>"],
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-grape",
        allowedFileTypes: ['image']
    });



	



    // ------------------- 05. Scroll To Top ------------------
    // --------------------------------------------------------

    jQuery(document).ready(function() {

        var offset = 250;
        var duration = 800;

        jQuery('.nuwaitu-backtop').click(function(event) {
          event.preventDefault();
          jQuery('html, body').animate({scrollTop: 0}, duration);
          return false;
        });

    });




    // ------------------- 06. step by step -------------------
    // --------------------------------------------------------
    function resetActive(event, percent) {
        $(".progress-bar").css("width", percent + "%").attr("aria-valuenow", percent);
        $(".progress-completed").text(percent + "%");

        $("div").each(function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
            }
        });

        if (event.target.className == "nu-steps") {
            $(event.target).addClass("active");
        }
        else {
            $(event.target.parentNode).addClass("active");
        }
    }


     $('.nu-user-thumb').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 


$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('textarea:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("textarea[type='text']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    allPrevBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        $(".form-group").removeClass("has-error");
        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});



$(function() {
   
         $('.thumbnail-sortable').sortable({
            placeholderClass: 'col-md-2'
        });
     });





